package ai.teman.linebotservice.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.message.MessageContent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.postback.PostbackContent;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class LinebotServiceControllerTest {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private LineMessagingClient lineMessagingClient;

    private LinebotServiceController linebotServiceController;

    @BeforeEach
    void setup(){
        linebotServiceController = new LinebotServiceController(lineMessagingClient, restTemplate);
    }

    @Test
    void testGetSimpleRestTemplate() {
        assertNotNull(linebotServiceController.getSimpleRestTemplate());
    }

    @Test
    void testRandomHandleEvent() {
        LinebotServiceController mockLinebotServiceController = mock(LinebotServiceController.class);

        doCallRealMethod().when(mockLinebotServiceController).handleEvent(any());

        mockLinebotServiceController.handleEvent(null);
        verify(mockLinebotServiceController, times(0)).handleMessageEvent(any());
        verify(mockLinebotServiceController, times(0)).handlePostbackEvent(any());

        MessageEvent<MessageContent> messageEvent = new MessageEvent<>("a", null, null, null);
        mockLinebotServiceController.handleEvent(messageEvent);
        verify(mockLinebotServiceController, times(1)).handleMessageEvent(any());
        verify(mockLinebotServiceController, times(0)).handlePostbackEvent(any());

        PostbackEvent postbackEvent = new PostbackEvent("a", null, null, null);
        mockLinebotServiceController.handleEvent(postbackEvent);
        verify(mockLinebotServiceController, times(1)).handleMessageEvent(any());
        verify(mockLinebotServiceController, times(1)).handlePostbackEvent(any());
    }

    @Test
    void testMessageEvent() throws JsonProcessingException {
        LinebotServiceController mockLinebotServiceController = mock(LinebotServiceController.class);
        Source source = mock(Source.class);
        TextMessageContent textMessageContent = new TextMessageContent("id", "heyyy");
        MessageEvent<TextMessageContent> messageEvent = new MessageEvent("a", source,
                textMessageContent, null);

        doCallRealMethod().when(mockLinebotServiceController).handleMessageEvent(any());

        when(mockLinebotServiceController.processCommand(any(), any())).thenThrow(new RuntimeException());

        mockLinebotServiceController.handleMessageEvent(messageEvent);
        verify(mockLinebotServiceController, times(0)).sendReplyMessage(any(), any());
    }

    @Test
    void testPostbackEvent() throws JsonProcessingException {
        LinebotServiceController mockLinebotServiceController = mock(LinebotServiceController.class);
        Source source = mock(Source.class);
        PostbackContent postbackContent = new PostbackContent("a", null);
        PostbackEvent postbackEvent = new PostbackEvent(mockLinebotServiceController.lineOfficialReplyToken,
                source, postbackContent, null);

        doCallRealMethod().when(mockLinebotServiceController).handlePostbackEvent(any());

        mockLinebotServiceController.handlePostbackEvent(postbackEvent);
        verify(mockLinebotServiceController, times(0)).processCommand(any(), any());

        postbackEvent = new PostbackEvent("a", source, postbackContent, null);
        TextMessage textMessage = new TextMessage("heyyyy");
        when(mockLinebotServiceController.processCommand(any(), any())).thenReturn(textMessage);

        mockLinebotServiceController.handlePostbackEvent(postbackEvent);
        verify(mockLinebotServiceController, times(1))
                .sendReplyMessage("a", textMessage);

        when(mockLinebotServiceController.processCommand(any(), any())).thenThrow(new RuntimeException());

        mockLinebotServiceController.handlePostbackEvent(postbackEvent);
        verify(mockLinebotServiceController, times(1))
                .sendReplyMessage("a", textMessage);
    }

    @Test
    void testBanguninMicroservices() {
        LinebotServiceController mockLinebotServiceController = mock(LinebotServiceController.class);
        TextMessageContent textMessageContent = new TextMessageContent("idnya", "/helow");
        MessageEvent<TextMessageContent> messageEvent = new MessageEvent<>("myreplytoken", null,
                textMessageContent, null);

        doCallRealMethod().when(mockLinebotServiceController).banguninMicroservices(any());
        when(mockLinebotServiceController.getSimpleRestTemplate()).thenReturn(mock(RestTemplate.class));

        mockLinebotServiceController.banguninMicroservices(messageEvent);
        verify(mockLinebotServiceController, times(1)).getSimpleRestTemplate();
        verify(mockLinebotServiceController, times(1)).sendReplyMessage(any(), any());

        mockLinebotServiceController.banguninMicroservices(null);
        verify(mockLinebotServiceController, times(2)).getSimpleRestTemplate();
        verify(mockLinebotServiceController, times(1)).sendReplyMessage(any(), any());
    }


    @Test
    void testHandleEvent() throws JsonProcessingException {
        //setup mocks
        Source source = mock(Source.class);
        TextMessageContent textMessageContent = new TextMessageContent("tmc1", "message");
        MessageEvent<TextMessageContent> messageEvent = new MessageEvent<TextMessageContent>(
                "a", source, textMessageContent, null
        );


        //test when handleEvent is called restTemplate should be called with correct url and arguments
        ObjectMapper mapper = new ObjectMapper();
        mapper.setAnnotationIntrospector(new JacksonAnnotationIntrospector() {

            @Override
            public boolean hasIgnoreMarker(final AnnotatedMember m) {
                return super.hasIgnoreMarker(m) || m.getName().contains("Mockito");
            }
        });
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

        String jsonMessage = mapper.writeValueAsString("message");
        String jsonSource = mapper.writeValueAsString(source);
        List<String> listJson = new ArrayList<>();
        listJson.add(jsonMessage);
        listJson.add(jsonSource);

        linebotServiceController.handleEvent(messageEvent);
        verify(restTemplate).postForObject(
                "http://command-service/process/",
                new HttpEntity<List<String>>(listJson), Message.class);

        //test when handleEvent is called it should call replyMessage in lineMessagingClient
        Message messageFromHandleEvent = new TextMessage("Hasil process");
        when(restTemplate.postForObject(anyString(), any(HttpEntity.class), eq(Message.class))).
                thenReturn(messageFromHandleEvent);

        CompletableFuture completableFuture = mock(CompletableFuture.class);
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).
                thenReturn(completableFuture);

        linebotServiceController.handleEvent(messageEvent);
        verify(lineMessagingClient).replyMessage(any(ReplyMessage.class));

    }
}

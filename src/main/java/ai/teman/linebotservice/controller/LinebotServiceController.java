package ai.teman.linebotservice.controller;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.response.BotApiResponse;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;
import java.util.logging.Logger;

@LineMessageHandler
@RestController
public class LinebotServiceController {

    private static final Logger LOGGER = Logger.getLogger(LinebotServiceController.class.getName());

    private LineMessagingClient lineMessagingClient;

    protected String lineOfficialReplyToken = "ffffffffffffffffffffffffffffffff";

    private RestTemplate restTemplate;

    @Autowired
    public LinebotServiceController(LineMessagingClient lineMessagingClient, RestTemplate restTemplate) {
        this.lineMessagingClient = lineMessagingClient;
        this.restTemplate = restTemplate;
    }

    protected RestTemplate getSimpleRestTemplate() {
        return new RestTemplate();
    }

    protected void handleMessageEvent(MessageEvent messageEvent) {
        try {
            Source chatSource = messageEvent.getSource();
            String replyToken = messageEvent.getReplyToken();

            if (replyToken.equals(lineOfficialReplyToken)) {
                LOGGER.info("LINE VERIFICATION RECEIVED");
            } else {
                String message = ((TextMessageContent) messageEvent.getMessage()).getText();
                Message commandResult = processCommand(message, chatSource);
                if (commandResult != null) {
                    sendReplyMessage(replyToken, commandResult);
                }
            }
        } catch (Exception e) {
            LOGGER.warning(e.getMessage());
            banguninMicroservices(messageEvent);
        }
    }

    protected void handlePostbackEvent(PostbackEvent postbackEvent) {
        try {
            Source chatSource = postbackEvent.getSource();
            String replyToken = postbackEvent.getReplyToken();

            if (replyToken.equals(lineOfficialReplyToken)) {
                LOGGER.info("LINE VERIFICATION RECEIVED");
            } else {
                String command = postbackEvent.getPostbackContent().getData();
                Message commandResult = processCommand(command, chatSource);
                if (commandResult != null) {
                    sendReplyMessage(replyToken, commandResult);
                }
            }
        } catch (Exception e) {
            LOGGER.warning(e.getMessage());
        }
    }

    @EventMapping
    public void handleEvent(Event event) {
        if (event instanceof MessageEvent) {
            handleMessageEvent((MessageEvent) event);
        } else if (event instanceof PostbackEvent) {
            handlePostbackEvent((PostbackEvent) event);
        } else {
            LOGGER.info("UNKNOWN EVENT RECEIVED");
        }
    }

    protected void banguninMicroservices(MessageEvent messageEvent) {
        RestTemplate simpleRestTemplate = getSimpleRestTemplate();
        try {
            LOGGER.info("MEMBANGUNKAN BOT...");
            LOGGER.info(simpleRestTemplate.getForObject("https://temanai.herokuapp.com/hello/", String.class));

            String message = ((TextMessageContent) messageEvent.getMessage()).getText();
            if (message.charAt(0) == '/' && message.length() > 1) {
                String replyToken = messageEvent.getReplyToken();
                Message toReply = new TextMessage("Membangunkan bot...");
                sendReplyMessage(replyToken, toReply);
            }
        } catch (Exception e) {
            LOGGER.severe(e.getMessage());
        }
    }

    protected Message processCommand(String message, Source chatSource) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        //configuring for test
        //kalo didelete, jadi error infinite recursion karena source harus dimock di test
        //references:
        //https://stackoverflow.com/questions/22851462/infinite-recursion-when-serializing-objects-with-jackson-and-mockito
        mapper.setAnnotationIntrospector(new JacksonAnnotationIntrospector() {

            @Override
            public boolean hasIgnoreMarker(final AnnotatedMember m) {
                return super.hasIgnoreMarker(m) || m.getName().contains("Mockito");
            }
        });
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

        String jsonMessage = mapper.writeValueAsString(message);
        String jsonSource = mapper.writeValueAsString(chatSource);
        List<String> listJson = new ArrayList<>();
        listJson.add(jsonMessage);
        listJson.add(jsonSource);
        return restTemplate.postForObject("http://command-service/process/",
                new HttpEntity<List<String>>(listJson), Message.class);
    }

    protected void sendReplyMessage(String replyToken, Message toSend) {
        Consumer<BotApiResponse> isDone = futureResult -> {
            for (String result : futureResult.getDetails()) {
                LOGGER.info(result);
            }
        };

        CompletableFuture<BotApiResponse> future = lineMessagingClient.
                replyMessage(new ReplyMessage(replyToken, toSend));
        future.thenAccept(isDone);
    }

    @PostMapping("/push-message/")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public void receivePushMessage(@RequestBody List<String> jsonList){
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        String userID = null;
        String message = null;
        try {
            userID = mapper.readValue(jsonList.get(0), String.class);
            message = mapper.readValue(jsonList.get(1), String.class);
            pushMessage(userID, message);
        } catch (JsonProcessingException e){
            LOGGER.warning("Error ketika memparse json. Kontak developer.");
        }
    }

    protected void pushMessage(String senderId, String message) {
        TextMessage textMessage = new TextMessage(message);
        try {
            lineMessagingClient
                    .pushMessage(new PushMessage(senderId, textMessage)).get();
        } catch (InterruptedException | ExecutionException e) {
            LOGGER.warning("Ada error saat ingin membalas chat");
            Thread.currentThread().interrupt();
        }
    }
}
